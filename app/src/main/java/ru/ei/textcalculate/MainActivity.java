package ru.ei.textcalculate;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

/**
 * Программа реализует сложение двух элементов
 */
public class MainActivity extends AppCompatActivity {

    private EditText enterText1;
    private EditText enterText2;
    private String firstOperand;
    private String secondOperand;
    private Boolean correct = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        enterText1 = findViewById(R.id.enter_text_first_operand);
        enterText2 = findViewById(R.id.enter_text_second_operand);
    }

    public void onSendClick(View view) {
        correct = true;
        firstOperand = enterText1.getText().toString();
        secondOperand = enterText2.getText().toString();
        if(verification()){
            sendIntent(new MathOperation(Integer.parseInt(firstOperand), Integer.parseInt(secondOperand), '+').toString());
        }
    }

    private void sendIntent(String operation) {
        Intent intent = new Intent(this, ShowResult.class);
        intent.putExtra("operation", operation);
        startActivity(intent);
    }

    private boolean verification() {
        correct = true;
        if (!isNumber(secondOperand)) {
            enterText2.setError("введите корректное число");
            correct = false;
        }
        if (!isNumber(firstOperand)) {
            enterText1.setError("введите корректное число");
            correct = false;
        }
        return correct;
    }

    private boolean isNumber(String text){
        return text.matches("[+-]?[0-9]+");
    }

}


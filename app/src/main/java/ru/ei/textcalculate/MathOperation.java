package ru.ei.textcalculate;

import androidx.annotation.NonNull;

public class MathOperation {
    private int firstOperand;
    private int secondOperand;
    private char operation;
    private int result;

    public MathOperation(int firstOperand, int secondOperand, char operation){
        this.firstOperand = firstOperand;
        this.secondOperand = secondOperand;
        this.operation = operation;
        calculate();
    }

    private int calculate(){
        result = 0;
        if (operation == '+'){
            result = firstOperand + secondOperand;
        }
        return result;
    }

    @NonNull
    @Override
    public String toString() {
        String secondOperand = (this.secondOperand < 0) ? "(" + this.secondOperand + ")" : String.valueOf(this.secondOperand);
        return firstOperand + " " + operation + " " + secondOperand + " = " + result;
    }

}

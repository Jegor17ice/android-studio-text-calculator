package ru.ei.textcalculate;

import android.os.Bundle;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;

/**
 *  Класс создания нового активити с результатами операации
 */
public class ShowResult extends AppCompatActivity {
    private TextView textView;
    private String operation ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        createLayout();
        getIntentData();
        show();
    }

    private void show() {
        textView.setText(operation);
    }

    private void getIntentData() {
        operation = getIntent().getStringExtra("operation");
    }

    private void createLayout() {
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        linearLayout.setGravity(Gravity.CENTER);
        textView = new TextView(this);
        textView.setTextSize(50);
        textView.setGravity(Gravity.CENTER);
        setContentView(linearLayout);
        linearLayout.addView(textView);
    }


}

